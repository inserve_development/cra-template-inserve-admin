import { CssBaseline, ThemeProvider } from '@material-ui/core'
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom'
import Admin from './routes/Admin'
import Auth from './routes/Auth/Auth'
import theme from './theme'

function App() {
  return (
   <ThemeProvider theme={theme}>
     <CssBaseline />
     <Router>
       <Switch>
         <Route exact path='/' component={Auth}/>
         <Route path='/admin' component={Admin}/>
       </Switch>
     </Router>
   </ThemeProvider>
  )
}

export default App
