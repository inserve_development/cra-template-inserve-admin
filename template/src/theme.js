import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#46b478',
      contrastText: 'white'
    },
    secondary: {
      main: '#315440'
    },
    background: {
      default: '#f9f9fc'
    }
  },
})

theme.shadows[1] = '0 0 14px 0 rgba(53,64,82,.07)'
theme.shape.borderRadius = 10

export default theme