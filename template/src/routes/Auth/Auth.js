import { Container, Grid, makeStyles, Paper, Typography } from '@material-ui/core'
import MuiLink from '@material-ui/core/Link'
import { Field, Form, Formik } from 'formik'
import * as Yup from 'yup'
import TextField from '../../components/fields/TextField'
import PasswordField from '../../components/fields/PasswordField'
import Button from '../../components/Button'
import Logo from '../../images/logo'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    minHeight: '100vh'
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: 20
  },
  brand: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  button: {
    margin: theme.spacing(2, 0, 2),
    height: 56
  }
}))

const Auth = ({ history, location }) => {
  const classes = useStyles()

  return (
  <Container component='main' maxWidth='xs' className={classes.container}>
    <Paper className={classes.paper}>
      <Logo width={150} height={150} />
      <Typography
        component='h1'
        variant='h4'
        className={classes.brand}
      >
        Inserve Admin  
      </Typography>
      <Formik
        initialValues={{}}
        validationSchema={Yup.object().shape({
          email: Yup.string()
            .email('Felaktig e-postadress')
            .required('Ange e-postadress'),
          password: Yup.string()
            .required('Du måste ange ett lösenord')
        })}
        onSubmit={async values => {
          debugger
        }}
      >
        {({
          isSubmitting
        }) => (
          <Form className={classes.form}>
            <Field
              color='primary'
              variant='outlined'
              margin='normal'
              type='email'
              name='email'
              fullWidth
              autoFocus
              component={TextField}
              label='Användarnamn'
            />
            <Field
              color='primary'
              variant='outlined'
              margin='normal'
              type='password'
              name='password'
              fullWidth
              component={PasswordField}
              label='Lösenord'
            />
            <Button
              type='submit'
              size='large'
              fullWidth
              variant='contained'
              disabled={isSubmitting}
              loading={isSubmitting}
              color='primary'
              className={classes.button}
            >
              Logga in
            </Button>
          </Form>
        )}
      </Formik>
      <Grid container>
        <Grid item xs>
          <MuiLink>
            Glömt lösenord
          </MuiLink>
        </Grid>
      </Grid>
    </Paper>
  </Container>
  )
}

export default Auth