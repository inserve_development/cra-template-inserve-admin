import { AppBar, Container, Drawer, Grid, IconButton, List, ListItem, ListItemIcon, ListItemText, Paper, Toolbar, Tooltip } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { ChevronLeft, PowerSettingsNew } from '@material-ui/icons'
import MenuIcon from '@material-ui/icons/Menu'
import clsx from 'clsx'
import { useState } from 'react'
import { Link, Route } from 'react-router-dom'
import { CSSTransition } from 'react-transition-group'
import Logo from '../../images/logo'
import routes from './routes'
const drawerWidth = 240

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex'
  },
  appBar: {
    backgroundColor: theme.palette.background.default,
    color: theme.palette.secondary.main,
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  toolbar: {
    paddingRight: 24,
    paddingLeft: 20
  },
  menuButton: {
    marginRight: 36
  },
  menuButtonHidden: {
    display: 'none'
  },
  grow: {
    flexGrow: 1
  },
  drawer: {
    position: 'relative',
    whiteSpace: 'nowrap',
    backgroundColor: theme.palette.background.default,
    color: theme.palette.secondary.main,
    borderRight: 'none',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClosed: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9)
    }
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  icon: {
    color: 'inherit'
  },
  list: {
    color: theme.palette.secondary.main
  },
  link: {
    textDecoration: 'none',
    color: 'inherit'
  },
  button: {
    '&:hover': {
      backgroundColor: theme.palette.background.default
    }
  },
  iconPaper: {
    backgroundColor: theme.palette.background.default,
    boxShadow: 'none',
    display: 'flex',
    padding: 3,
    transition: 'all .3s cubic-bezier(.29,1.09,.74,1.3)',
    transform: 'scale(1)',
    '&.icons-enter': {
      transform: 'scale(0.1) translateY(10px)',
    },
    '&.icons-enter-done': {
      transform: 'scale(1) translateY(0)'
    },
    '&.icons-exit': {
      transform: 'scale(1.1) translateY(-10px)'
    },
    '&.icons-exit-done': {
      transform: 'scale(1) translateY(0)'
    }
  },
  selectedIconPaper: {
    backgroundColor: theme.palette.primary.main,
    boxShadow: '0 0 14px 0 rgba(53,64,82,.17)',
    color: theme.palette.primary.contrastText
  },
  textAnimation: {
    transition: 'transform .2s cubic-bezier(.29,1.09,.74,1.3), opacity .2s cubic-bezier(0,.99,.7,.69)',
    opacity: 0,
    '&.text-enter': {
      transform: 'translateX(-10px)'
    },
    '&.text-enter-done': {
      transform: 'translateX(0)',
      opacity: 1
    },
    '&.text-exit': {
      transform: 'translateX(-10px)'
    }
  },
  primaryListText: {
    display: 'flex',
    transition: 'all .3s cubic-bezier(.29,1.09,.74,1.3)',
    transform: 'scale(1)',
    '&.texts-enter': {
      transform: 'scale(0.7) translateY(10px)',
      opacity: 0.1,
    },
    '&texts-enter-done': {
      transform: 'scale(1) translateY(0)'
    },
    '&.texts-exit': {
      transform: 'scale(1.1) translateY(-10px)',
      opacity: 1
    },
    '&texts-exit-done': {
      transform: 'scale(1) translateY(0)'
    }
  },
  content: {
    flexGrow: 1,
    height: '100vh',
    overflow: 'auto'
  },
  appbarSpacer: theme.mixins.toolbar,
  container: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2),
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3)
  }
}))


const Admin = ({ match, location, history }) => {
  const classes = useStyles()
  const [open, setOpen] = useState(false)
  
  const checkSelected = (path) => `${match.url}${path}` === location.pathname

  return (
    <div className={classes.root}>
      <AppBar
        position='absolute'
        elevation={0}
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open
        })}
      >
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge='start'
            color='inherit'
            aria-label='open-menu'
            onClick={() => setOpen(true)}
            className={clsx(classes.menuButton, {
              [classes.menuButtonHidden]: open
            })}
          >
            <MenuIcon />
          </IconButton>
          <div className={classes.grow} />
          <Tooltip title='Logga ut' aria-label='logout'>
            <IconButton aria-label='logout' color='inherit'>
              <PowerSettingsNew />
            </IconButton>
          </Tooltip>
        </Toolbar>
      </AppBar>
      <Drawer
        variant='permanent'
        classes={{
          paper: clsx(classes.drawer, {
            [classes.drawerClosed]: !open
          })
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <div />
          <IconButton
            classes={{ root: classes.icon }}
            onClick={() => setOpen(false)}
          >
            <ChevronLeft />
          </IconButton>
        </div>
        <List
          classes={{
            root: classes.list
          }}
        >
          {routes.map((route, index) => (
            <Link
              key={route.id}
              to={`${match.url}${route.path}`}
              className={classes.link}
            >
              <ListItem
                button
                disableRipple
                className={clsx(classes.button, {
                  [classes.selected]: checkSelected(route.path)
                })}
              >
                <ListItemIcon
                  classes={{ root: classes.icon }}
                >
                  <CSSTransition
                    in={checkSelected(route.path)}
                    className={clsx(classes.iconPaper, {
                      [classes.selectedIconPaper]: checkSelected(route.path)
                    })}
                    timeout={{ enter: 300, exit: 300 }}
                    classNames='icons'
                  >
                    <Paper className='icons'>
                      {route.icon}
                    </Paper>
                  </CSSTransition>
                </ListItemIcon>
                <CSSTransition
                  in={open}
                  className={classes.textAnimation}
                  timeout={{ enter: 200, exit: 200 }}
                  classNames='text'
                >
                  <ListItemText
                    primary={
                      <CSSTransition
                        in={checkSelected(route.path)}
                        className={classes.primaryListText}
                        timeout={{ enter: 300, exit: 300 }}
                        classNames='texts'
                      >
                        <span className='texts'>
                          {route.text}
                        </span>
                      </CSSTransition>
                    }
                    className='text'
                    style={{
                      transitionDelay: `${index - 1} * 30}ms`
                    }}
                  />
                </CSSTransition>
              </ListItem>
            </Link>
          ))}
        </List>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appbarSpacer} />
        <Container maxWidth={false} className={classes.container}>
          <Grid container spacing={2}>
            {routes.map(route => (
              <Route
                key={route.id}
                exact={route.exact}
                path={`${match.url}${route.path}`}
                component={route.component}
              />
            ))}
          </Grid>
        </Container>
      </main>
    </div>
  )
}

export default Admin