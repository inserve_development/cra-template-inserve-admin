import { v4 as uuid } from 'uuid'
import DashboardIcon from '@material-ui/icons/Web'
import Dashboard from './Dashboard'

const routes = [
  {
    id: uuid(),
    path: '',
    icon: <DashboardIcon />,
    text: 'Skrivbord',
    component: Dashboard,
    exact: true
  }
]

export default routes