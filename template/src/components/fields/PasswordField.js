import { useState } from 'react'

import {
  TextField,
  InputAdornment,
  IconButton
} from '@material-ui/core'

import {
  Visibility,
  VisibilityOff
} from '@material-ui/icons'

const PasswordField = ({
  field: { ...fields },
  form: { touched, errors, ...rest },
  ...props
}) => {
  const [show, setShow] = useState(false)
  return (
    <TextField
      {...props}
      {...fields}
      type={!show ? 'password' : 'text'}
      error={Boolean(touched[fields.name] && errors[fields.name])}
      helperText={touched[fields.name] && errors[fields.name]}
      InputProps={{
        endAdornment: (
          <InputAdornment position='end'>
            <IconButton
              onClick={() => setShow(!show)}
              tabIndex={-1}
            >
              {!show ? <Visibility /> : <VisibilityOff />}
            </IconButton>
          </InputAdornment>
        )
      }}
    />
  )
}

export default PasswordField