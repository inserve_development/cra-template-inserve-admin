import { Button, CircularProgress } from '@material-ui/core'
import { makeStyles, ThemeProvider } from '@material-ui/core/styles'
import theme from '../theme'

const useStyles = makeStyles(theme => ({
  wrapper: {
    position: 'relative',
  },
  progress: {
    color: 'white',
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12
  }
}))

const AvisitButton = ({
  children,
  loading,
  ...props
}) => {
  const classes = useStyles()
  return (
    <ThemeProvider theme={theme}>
      <div className={classes.wrapper}>
        <Button
          {...props}
        >
          {children}
        </Button>
        {loading && <CircularProgress size={24} className={classes.progress} />}
      </div>
    </ThemeProvider>
  )
}

export default AvisitButton