import { Grid, makeStyles, Typography } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  header: {
    paddingBottom: 30
  }
}))

const Dashboard = props => {

  const classes = useStyles()

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Typography variant='h4' className={classes.header}>
          Välkommen
        </Typography>
      </Grid>
    </Grid>
  )
}

export default Dashboard