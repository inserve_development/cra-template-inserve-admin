const logo = ({width = 70, height = 70, fill = '#46b478'}) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    viewBox="0 0 100 100"
    width={width}
    height={height}
  >
    <path
      fill={fill}
      d="
        M 0, 50
        A 1 1, 0, 0 0, 100, 50
        A 1 1, 0, 0 0, 0, 50
        L 15, 50
        A 1 1, 0, 0 1, 85, 50
        A 1 1, 0, 0 1, 15, 50
        Z
        M 30 50
        A 1 1, 0, 0 0, 70, 50
        A 1 1, 0, 0 0, 30, 50
        Z
      "    
    />
  </svg>
)

export default logo