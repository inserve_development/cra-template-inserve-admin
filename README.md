# CRA Template Inserve Admin

This is a create-react-app template to bootstrap a Inserve Admin project and install dependencies.

-

## Usage 

### In terminal run:

`npx create-react-app [your-project-name] --template inserve-admin`

-

## Development

* Make changes
* Run `yarn refresh` to empty template folder and copy needed files

### Publish to npm

* `npm login`
* `npm publish --access public` 

 


 